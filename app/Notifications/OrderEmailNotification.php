<?php

namespace App\Notifications;

use App\Models\Order;
use App\Models\User;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class OrderEmailNotification extends Notification implements ShouldQueue
{
    use Queueable;
    /**
     * @var Order
     */
    public $order;
    /**
     * @var User
     */
    public $user;

    /**
     * Create a new notification instance.
     *
     * @param Order $order
     * @param string $user
     */
    public function __construct(Order $order, string $user)
    {
        $this->order = $order;
        $this->user = $user;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->line('Dear ' . $this->user)
                    ->line('Your order has been placed successfully')
                    ->line('Your order ID is: ' . $this->order->id)
                    ->action('View Details', route('order.details', $this->order->id))
                    ->line('Thank you for using our application!');
    }

}
