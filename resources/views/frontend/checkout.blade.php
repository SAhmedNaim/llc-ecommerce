@extends('frontend.layouts.master')

@section('main')

    <div class="container">
        <br/>
        <p class="text-center">Checkout</p>
        <hr/>

        @guest()

            <div class="alert alert-info">
                You need to <a href="{{ route('login') }}">Login</a> first to complete your order.
            </div>

        @else

            You are ordering as {{ auth()->user()->name }}

        @endguest

    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-4 order-md-2 mb-4">
                <h4 class="d-flex justify-content-between align-items-center mb-3">
                    <span class="text-muted">Your cart</span>
                    <span class="badge badge-secondary badge-pill">{{ count($cart) }}</span>
                </h4>
                <ul class="list-group mb-3">

                    @foreach($cart as $key => $product)

                        <li class="list-group-item d-flex justify-content-between lh-condensed">
                            <div>
                                <h6 class="my-0">{{ $product['title'] }}</h6>
                                <small class="text-muted">Quantity: {{ $product['quantity'] }}</small>
                            </div>
                            <span class="text-muted">{{ number_format($product['total_price'], 2) }}</span>
                        </li>

                    @endforeach

                    <li class="list-group-item d-flex justify-content-between">
                        <span>Total (BDT)</span>
                        <strong>TK. {{ $total }}</strong>
                    </li>
                </ul>
            </div>
            <div class="col-md-8 order-md-1">

                @include('frontend.partials._message')

                <h4 class="mb-3">Billing address</h4>

                @auth()
                    <form class="needs-validation" action="{{ route('order') }}" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-md-12 mb-3">
                            <label for="customer_name">Customer Name <span class="text-muted">(Required)</span></label>
                            <input type="text" class="form-control" name="customer_name" id="customer_name" placeholder="Enter customer name" value="{{ auth()->user()->name }}" required="">
                        </div>
                    </div>

                    <div class="mb-3">
                        <label for="customer_phone_number">Customer Phone Number <span class="text-muted">(Required)</span></label>
                        <input type="text" class="form-control" name="customer_phone_number" id="customer_phone_number" value="{{ auth()->user()->phone_number }}">
                    </div>

                    <div class="mb-3">
                        <label for="address">Address <span class="text-muted">(Required)</span></label>
                        <textarea class="form-control" name="address" id="address" placeholder="Enter address here.." required=""></textarea>
                    </div>

                    <div class="row">
                        <div class="col-md-6 mb-3">
                            <label for="city">City <span class="text-muted">(Required)</span></label>
                            <input type="text" class="form-control" name="city" id="city" placeholder="">
                        </div>
                        <div class="col-md-6 mb-3">
                            <label for="postal_code">Postal Code <span class="text-muted">(Required)</span></label>
                            <input type="text" class="form-control" name="postal_code" id="postal_code" placeholder="">
                        </div>
                    </div>
                    <hr class="mb-4">
                    <button class="btn btn-primary btn-lg btn-block" type="submit">Continue to checkout</button>
                </form>
                @endauth

            </div>
        </div>
    </div>

@endsection
