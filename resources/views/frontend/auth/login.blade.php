@extends('frontend.layouts.master')

@section('main')

    <div class="container">
        <br/>
        <p class="text-center">Login</p>
        <hr/>

        @include('frontend.partials._message')

        <form action="{{ route('login') }}" method="post" class="form">
            @csrf

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" required class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" required class="form-control">
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" type="submit">Login</button>
            </div>
        </form>

    </div>

@endsection
