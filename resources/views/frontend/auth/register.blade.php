@extends('frontend.layouts.master')

@section('main')

    <div class="container">
        <br/>
        <p class="text-center">Register</p>
        <hr/>

        @include('frontend.partials._message')

        <form action="{{ route('register') }}" method="post" class="form">
            @csrf
            <div class="form-group">
                <label for="name">Full Name</label>
                <input type="text" name="name" id="name" value="{{ old('name') }}" required class="form-control">
            </div>

            <div class="form-group">
                <label for="email">Email</label>
                <input type="email" name="email" id="email" value="{{ old('email') }}" required class="form-control">
            </div>

            <div class="form-group">
                <label for="phone_number">Phone Number</label>
                <input type="text" name="phone_number" id="phone_number" value="{{ old('phone_number') }}" required class="form-control">
            </div>

            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" name="password" id="password" required class="form-control">
            </div>

            <div class="form-group">
                <button class="btn btn-block btn-success" type="submit">Register</button>
            </div>
        </form>

    </div>


@endsection
